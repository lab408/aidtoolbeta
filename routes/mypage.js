var express = require('express');
//aidtool
var router = express.Router();

wait=require('wait.for');
const sqlite3 = require('sqlite3').verbose();
var async = require('async');
var dbSetUp=require('./sqliteSetUp');
var querystring = require('querystring');
var http=require('http');
var Buffer=require('Buffer');
var dialog = require('dialog');

var localDb;
router.get('/',function(req,res,next){
async.waterfall([
	function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
dbSetUp.SetUp(localDb);
next(null,localDb);
},
function(localDb,next){
localDb.serialize(function(){
	let sqlQuery="SELECT * FROM GOOGLE";
localDb.all(sqlQuery,[],function(err,rows){
	
		if(rows.length){
			next(err,localDb,rows[0].Email);
			
		}else{
			next(err,localDb,"");
		}
	
});
});
},
function(localDb,googleEmail,next){
  
 localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";
localDb.all(sqlQuery,[],function(err,rows){
	
		if(rows.length){
			res.render('mypage',{
				"googleEmail":googleEmail,
				"id_type":rows[0].LoginType
			});
			
		}else{
			res.render('mypage',{
				"googleEmail":googleEmail
			});
		}
		next(err);
	
});
});
}
],
function(err){
if(err){
	console.log(err);
}
}

);
});
router.post('/updategoogleemail',function(req,res,next){

	async.waterfall([
		function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM GOOGLE";
localDb.all(sqlQuery,[],function(err,rows){
		console.log(rows);
		if(rows.length){
			next(err,localDb,rows[0].Email);
			
		}else{
			next(err,localDb,null);
		}
	
});
});
},
function(localDb,googleEmail,next){
	if(googleEmail!=null){
 let data = [req.body.googleEmail,googleEmail];
 let sqlQuery='UPDATE GOOGLE SET Email=? WHERE Email=?'
 localDb.run(sqlQuery,data,function(err){
 	console.log("update google email");
 	next(err);
 });
}else{

	let stmt=localDb.prepare("INSERT INTO GOOGLE VALUES(?)");
	stmt.run(req.body.googleEmail);
	stmt.finalize();
	
	next(null);

}
//res.redirect('/mypage');
}
		],
	function(err){
		console.log("should go in");
		if(err){
			console.log(err);
		}
		console.log('backkk');
		res.redirect('back');
	}
		);
});
router.post('/updatelogintype',function(req,res){

	async.waterfall([
		function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";
localDb.all(sqlQuery,[],function(err,rows){
		console.log(rows);
		if(rows.length){
			next(err,localDb,rows[0].Id);
			
		}else{
			next(err,localDb,null);
		}
	
});
});
},
function(localDb,Id,next){
	console.log(Id);
	if(Id!=null){
 let data = [req.body.logintype,Id];
 let sqlQuery='UPDATE USER SET LoginType=? WHERE Id=?'
 localDb.run(sqlQuery,data,function(err){
 	
 	next(err);
 });
}else{

	console.log("please create first user");
	
	next(null);

}
//res.redirect('/mypage');
}
		],
	function(err){
		if(err){
			console.log(err);
		}
		console.log('backkk');
		res.redirect('/mypage');
	}
		);
});
module.exports = router;