var express = require('express');
//aidtool
var bufferReversed=require('buffer-reverse');
const secp256k1 = require('secp256k1');
var router = express.Router();
const sha256=require('sha256');
const uuidV4 = require('uuid/v4');
var bitcoinjsLib=require('bitcoinjs-lib');
var crypto_ecdsa=require('./crypto_ecdsa');
var ec_curve = "secp256k1";
var wait=require('wait.for');
const sqlite3 = require('sqlite3').verbose();
var async = require('async');
var dbSetUp=require('./sqliteSetUp');
var querystring = require('querystring');
var http=require('http');
var Buffer=require('Buffer');
var dialog = require('dialog');
var EC = require("elliptic").ec;
var ec = new EC(ec_curve);
var bitcoin_rpc = require('node-bitcoin-rpc');
var localDb;
var getVar=require('../import.js');
/* GET  mypage. */
var config=require('../config.json');
var contractTx=getVar.readContractTx();
console.log(contractTx);
var port=getVar.readPort();
console.log(port);
var rpcUser=getVar.readRPC_user();
var rpcPass=getVar.readRPC_password();
var host=getVar.readIP();
router.get('/',function(req,res,next){
//res.send(uuidV4());
async.waterfall([
	function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
dbSetUp.SetUp(localDb);
next(null,localDb);
},
function(localDb,next){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM GOOGLE";
localDb.all(sqlQuery,[],function(err,rows){
	
		if(rows.length){
			next(err,localDb,rows[0].Email);
			
		}else{
			next(err,localDb,"");
		}
	
});
});
},
function(localDb,googleEmail,next){
localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";
	var sqlQueryForHash="SELECT * FROM HASH";
	
	var hash="";
	
	

localDb.all(sqlQueryForHash,[],function(err,rows){
	if(err){
		console.log(err);
	}else{
		if(rows.length){
			hash=rows[0].Hash;
			};
		
	}
});
localDb.all(sqlQuery,[],function(err,rows){
	if(err){
		console.log(err);
	}else{
		if(rows.length){
			console.log("should have rows");
			console.log(rows[0]);
			var publicKey=rows[0].PublicKey;
			var reversedFirstHalfPub=bufferReversed(Buffer.from(publicKey.slice(2,publicKey.length/2+1),'hex'));
			var reversedSecondHalfPub=bufferReversed(Buffer.from(publicKey.slice(publicKey.length/2+1),'hex'));
			console.log(reversedFirstHalfPub);
			console.log(reversedSecondHalfPub)
			var reversedPub=reversedFirstHalfPub.toString('hex')+reversedSecondHalfPub.toString('hex');
			console.log(reversedPub);
			res.render('index',{
				hash:hash,
				userId:rows[0].Id,
				PublicKey:rows[0].PublicKey,
				id_type:rows[0].LoginType,
				googleEmail:googleEmail
			});
		}else{
			res.render('index');
		}
	}
});
});
next(null);
}
],
function(err){
if(err){
	console.log(err);
}
}

);
});

router.post('/createUser',function(req,res){
console.log('creating user');
var id=uuidV4().toString();

keys=crypto_ecdsa.getECKeyPair(req.body.password.toString(),id);
localDb.serialize(function(){
	let stmt=localDb.prepare("INSERT INTO USER VALUES(?,?,?,?)");
	stmt.run(id,keys.ecpubhex.toString(),keys.ecprvhex.toString(),"0");
	stmt.finalize();
	res.redirect('back');
});

});
router.get('/createsignature',function(req,res){
	console.log(req.query.shaMsg);
	
async.waterfall([
	function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
dbSetUp.SetUp(localDb);
next(null,localDb);
},
function(localDb,next){
	var PrivateKey;
localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";

localDb.all(sqlQuery,[],function(err,rows){
	
	if(err){
		console.log(err);
	}else{
		if(rows.length){
			PrivateKey=rows[0].PrivateKey;
			
			
			var pub=rows[0].PublicKey;
			var reversedFirstHalfPub=bufferReversed(Buffer.from(pub.slice(2,pub.length/2+1),'hex'));
			var reversedSecondHalfPub=bufferReversed(Buffer.from(pub.slice(pub.length/2+1),'hex'));
			console.log(reversedFirstHalfPub);
			console.log(reversedSecondHalfPub)
			var reversedPub=reversedFirstHalfPub.toString('hex')+reversedSecondHalfPub.toString('hex');
			console.log(reversedPub);
			console.log(pub.slice(2));
			console.log("public key above");
			//console.log(PrivateKey);
			console.log(req.query.shaMsg);
			var bufferMsg=Buffer.from(req.query.shaMsg,'hex');
	console.log(bufferMsg.toString('hex'));
	var normalizeSig=secp256k1.sign(bufferMsg,Buffer.from(PrivateKey,'hex')).signature;
	console.log(normalizeSig);
	var strNormalizeSig=normalizeSig.toString('hex');
	var reversedFirstHalfSig=bufferReversed(Buffer.from(strNormalizeSig.slice(0,strNormalizeSig.length/2),'hex'));
	var reversedSecondHalfSig=bufferReversed(Buffer.from(strNormalizeSig.slice(strNormalizeSig.length/2),'hex'));
	console.log(strNormalizeSig);
	var byteReversedSig=reversedFirstHalfSig.toString('hex')+reversedSecondHalfSig.toString('hex');
	console.log(byteReversedSig);
	console.log("signature");
	dialog.info(strNormalizeSig);
	
	
	res.redirect('back');
		}else{
			dialog.info('database error please try again');
			res.redirect('back');
		}
	}
});
});
next(null,PrivateKey);
}
],
function(err){
if(err){
	console.log(err);
}
}

);

});
router.post('/writehash',function(req,res,next) //write hash to smart contract, this is the only way to add hash , all below method only change the hash parameters user want to writes , won't write hash to the smart contract unless user click this
{
    var strTobeHash="";
	let sqlUpdated=0; // use this to make sure mysql update finish
    
	


        //d.update('1');//assume let my login is index 1
    async.waterfall([
	function(next){
localDb=new sqlite3.Database('./routes/aidLocalData.db',sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,function(err1){
	
		console.log("database Created");
	next(err1,localDb);
});
},
function(localDb,next){
dbSetUp.SetUp(localDb);
next(null,localDb);
},

function(localDb,next){
	
localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";
localDb.all(sqlQuery,[],function(err,rows){
	console.log(rows[0]);
		if(rows.length){
			strTobeHash+=rows[0].Id;
			strTobeHash+=rows[0].PublicKey;
			strTobeHash+=rows[0].LoginType;
			next(null,localDb,rows[0].LoginType);
		}else{
			res.redirect('/');
		}
	
});
});

},
function(localDb,loginType,next){
	if(loginType=='2'){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM GOOGLE";
localDb.all(sqlQuery,[],function(err,rows){
	
		if(rows.length){
			strTobeHash+=rows[0].Email;
			next(err,localDb);
			
		}else{
			console.log('no gmail!!!');
			res.redirect('/');
		}
	
});
});
}else{
	next(null,localDb);
}
},
function(localDb,next){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM USER";
localDb.all(sqlQuery,[],function(err,rows){
	
	next(err,localDb,rows[0].PublicKey,rows[0].PrivateKey);
	
});
});
	
    
},
function(localDb,publicKey,privateKey,next){
	var generatedHash=sha256(strTobeHash);

	 
    var reversedFirstHalfPub=bufferReversed(Buffer.from(publicKey.slice(2,publicKey.length/2+1),'hex'));
			var reversedSecondHalfPub=bufferReversed(Buffer.from(publicKey.slice(publicKey.length/2+1),'hex'));
			console.log(reversedFirstHalfPub);
			console.log(reversedSecondHalfPub)
			var reversedPub=reversedFirstHalfPub.toString('hex')+reversedSecondHalfPub.toString('hex');
			var bufferHash=Buffer.from(generatedHash,'hex');
	
	var normalizeSig=secp256k1.sign(bufferHash,Buffer.from(privateKey,'hex')).signature;
	console.log(normalizeSig);
	var strNormalizeSig=normalizeSig.toString('hex');
	var reversedFirstHalfSig=bufferReversed(Buffer.from(strNormalizeSig.slice(0,strNormalizeSig.length/2),'hex'));
	var reversedSecondHalfSig=bufferReversed(Buffer.from(strNormalizeSig.slice(strNormalizeSig.length/2),'hex'));
	console.log(strNormalizeSig);
	var byteReversedSig=reversedFirstHalfSig.toString('hex')+reversedSecondHalfSig.toString('hex');
	bitcoin_rpc.init("localhost", port, rpcUser, rpcPass);
	bitcoin_rpc.setTimeout(50000);
                
                let argvs=[contractTx,"sign_up",reversedPub,generatedHash,byteReversedSig];
                    bitcoin_rpc.call('callcontract', argvs, function (err, rpc_res) {
      //console.log(err);
      console.log(rpc_res);
      if(rpc_res)
        next(err,localDb,generatedHash);

          
    });
},
function(localDb,hash,next){
	localDb.serialize(function(){
	let sqlQuery="SELECT * FROM HASH";
localDb.all(sqlQuery,[],function(err,rows){
	
		if(rows.length){
			
			let data = [hash];
 let sqlQuery='UPDATE HASH SET Hash=?'
 localDb.run(sqlQuery,data,function(err){
 	console.log("update google email");
 	next(err);
 });
			
		}else{
			
    var sqlInsertChain="INSERT INTO HASH VALUES(?)";
	localDb.run(sqlInsertChain,[hash],function(err){
		
		next(err);
	});
	
		}
	
});
});
	next(err);
}


],
function(err){
if(err){
	console.log(err);
}
res.redirect('/');
}

);
    
	 //wait for mysqlupdate finish
   
}) ; 
module.exports = router;
