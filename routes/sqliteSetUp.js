const sqlite3 = require('sqlite3').verbose();

function SetUp(db){
var sqlCreateTableUser="CREATE TABLE IF NOT EXISTS USER (Id TEXT PRIMARY KEY,PublicKey TEXT,PrivateKey Text,LoginType TEXT)";
var sqlCreateTableGoogle="CREATE TABLE IF NOT EXISTS GOOGLE (Email TEXT PRIMARY KEY)";
var sqlCreateTableChain="CREATE TABLE IF NOT EXISTS CHAIN (Address TEXT PRIMARY KEY,Publickey TEXT,PrivateKey TEXT)";
var sqlCreateTableHash="CREATE TABLE IF NOT EXISTS HASH (Hash TEXT PRIMARY KEY)";
db.serialize(function(){
	db.run(sqlCreateTableUser,function(err){
		if(err){
			console.log(err);
		}else{
			console.log("user table created");
		}
	});
	db.run(sqlCreateTableHash,function(err){
		if(err){
			console.log(err);
		}else{
			console.log("Hash table created");
		}
	});
	db.run(sqlCreateTableGoogle,function(err){
		if(err){
			console.log(err);
		}else{
			console.log("google table created");
		}
	});
db.run(sqlCreateTableChain,function(err){
		if(err){
			console.log(err);
		}else{
			console.log("chain table created");
		}
	});
});
}

module.exports.SetUp=SetUp;
