var config = require("./config.json");

module.exports = {
  readPort: function() {
    return config.Port;
  },
  readRPC_user: function() {
    return config.RPC_user;
  },
  readRPC_password: function() {
    return config.RPC_password;
  },
  readIP: function() {
    return config.IP;
  },
  readLifetime: function() {
    return config.Lifetime;
  },
readContractTx:function(){
    return config.ContractTx;
  }
};
