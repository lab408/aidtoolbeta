var express  = require('express');
var cookieParser = require('cookie-parser');
var app = express();app.use(cookieParser('xxx'));   //配置cookie-parser中间件，xxx表示加密的随机数(相当于密钥) 
app.get("/",function(req,res){    
//console.log(req.cookies);  //获取未加密的cookie   
 console.log(req.signedCookies);   
 //获取加密的cookie    
 res.send("你好nodejs");}); 
 app.get("/set",function(req,res){   
  //设置cookie    
  res.cookie('userinfo','123',{maxAge:600000,signed:true}); 
   //signed 表示对cookie加密    
   res.send("设置cookie成功");
});
 app.listen(3001,'127.0.0.1');


