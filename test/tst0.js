var http = require("http");
var querystring = require("querystring");
var express=require('express');
var app=express();
app.get('/',function(req, res) {
    var data = querystring.stringify({
        username: "myname",
        password: " pass"
    });

    var options = {
        host: 'localhost',
        port: 5006,
        path: '/aidlogin',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(data)
        }
    };

    var req = http.request(options, function(res)
    {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log("body: " + chunk);
        });
    });
    req.write(data);
    req.end();
});
app.listen(8090);