#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ourcontract.h>
typedef struct {
	char hash[65]; //len
	char publicKey[129]; //len
	
} chainid;

static struct {
	     
	int chainidNums;
	chainid chainids[1000];
} state;
static void state_init()
{
	state.chainidNums = 0;
	out_clear();
}
static int find_publickey(const char *publicKey)
{
	//out_printf("0:finding address\n");
	
	int i,j;
	for (i = 0; i < state.chainidNums; i++) {
		err_printf("01:finding address\n");
		
		if (str_cmp(publicKey,state.chainids[i].publicKey,128) == 0) {
			//out_printf("1:address found!!\n");
			return i;
		}
	
	}
	//out_printf("1:address finding failed!!\n");
	return -1;
}
static int sign_up(const char *hash, const char *publicKey)
{
	int index;
	/* already signed up */
	out_printf("signing up\n");
	out_printf("%s\n",publicKey);
	//out_printf("address: %s\n",address);
	if ((index=find_publickey(publicKey)) != -1) 
	{
		
		str_printf(state.chainids[index].hash, 64, "%s", hash);
		//str_printf(state.chainids[index].address, 256, "%s",address);
		out_printf("hash: %s\n",state.chainids[index].hash);
		
	}

	
	else
	{
	if (state.chainidNums == 1000) return -1;/* number of users reaches upper bound */

	str_printf(state.chainids[state.chainidNums].hash, 65, "%s", hash);
	str_printf(state.chainids[state.chainidNums].publicKey, 129, "%s",publicKey);
	
	//out_printf("%s\n", state.users[state.user_count].name);
	out_printf("pub %s\n",state.chainids[state.chainidNums].publicKey);
	out_printf("hash: %s\n",state.chainids[state.chainidNums].hash);
	state.chainidNums++;
	}
	out_printf("chainidnums: %d\n",state.chainidNums);
	return 1;
}
int contract_main(int argc, char **argv)
{
	if (state_read(&state, sizeof(state)) == -1) {
		/* first time call */
		state_init();
		state_write(&state, sizeof(state));
		state_read(&state, sizeof(state));
	}
	err_printf("%s\n",argv[2]);
	err_printf("%s\n",argv[3]);
	err_printf("%s\n",argv[4]);
	int i;
	int j;
	unsigned char tmp_uchr;
	
	/*char char_pubdata[]="bb19abd074d4a2fe21c9fa3e4e8c4681c37580e2f31b3e3de7c6f9dd4447adc036c5037e3d292b10bc1b117355f8e469b9b64bcdc9d9d15de9132065f2723cd6";
	char char_msg[]="8cc9d01f238f62940645a5691e44dfe155162a79039e412c6d1380d5d6c14751";
	char char_sig[]="93372de8df3c5f35efb247e305fdcae18ec34a6518b3d0b0f2c6a4b8c046bb5714bdc64a973ea222531fdbaef8bb7a55e05db60e47bad67bac9dfd9d386caa16";*/
/*
0xBB,0x19,0xAB,0xD0,0x74,0xD4,0xA2,0xFE,0x21,0xC9,0xFA,0x3E,0x4E,0x8C,0x46,0x81,0xC3,0x75,0x80,0xE2,0xF3,0x1B,0x3E,0x3D,0xE7,0xC6,0xF9,0xDD,0x44,0x47,0xAD,0xC0,
0x36,0xC5,0x03,0x7E,0x3D,0x29,0x2B,0x10,0xBC,0x1B,0x11,0x73,0x55,0xF8,0xE4,0x69,0xB9,0xB6,0x4B,0xCD,0xC9,0xD9,0xD1,0x5D,0xE9,0x13,0x20,0x65,0xF2,0x72,0x3C,0xD6,


0x93,0x37,0x2D,0xE8,0xDF,0x3C,0x5F,0x35,0xEF,0xB2,0x47,0xE3,0x05,0xFD,0xCA,0xE1,0x8E,0xC3,0x4A,0x65,0x18,0xB3,0xD0,0xB0,0xF2,0xC6,0xA4,0xB8,0xC0,0x46,0xBB,0x57,
0x14,0xBD,0xC6,0x4A,0x97,0x3E,0xA2,0x22,0x53,0x1F,0xDB,0xAE,0xF8,0xBB,0x7A,0x55,0xE0,0x5D,0xB6,0x0E,0x47,0xBA,0xD6,0x7B,0xAC,0x9D,0xFD,0x9D,0x38,0x6C,0xAA,0x16
*/
if(str_cmp(argv[1],"sign_up",7)==0)
{
	err_printf("%s\n",argv[2]);
	err_printf("%s\n",argv[3]);
	err_printf("%s\n",argv[4]);
/*unsigned char ms[]={0x8C,0xC9,0xD0,0x1F,0x23,0x8F,0x62,0x94,0x06,0x45,0xA5,0x69,0x1E,0x44,0xDF,0xE1,0x55,0x16,0x2A,0x79,0x03,0x9E,0x41,0x2C,0x6D,0x13,0x80,0xD5,0xD6,0xC1,0x47,0x51};
unsigned char pubd[]={0xBB,0x19,0xAB,0xD0,0x74,0xD4,0xA2,0xFE,0x21,0xC9,0xFA,0x3E,0x4E,0x8C,0x46,0x81,0xC3,0x75,0x80,0xE2,0xF3,0x1B,0x3E,0x3D,0xE7,0xC6,0xF9,0xDD,0x44,0x47,0xAD,0xC0,0x36,0xC5,0x03,0x7E,0x3D,0x29,0x2B,0x10,0xBC,0x1B,0x11,0x73,0x55,0xF8,0xE4,0x69,0xB9,0xB6,0x4B,0xCD,0xC9,0xD9,0xD1,0x5D,0xE9,0x13,0x20,0x65,0xF2,0x72,0x3C,0xD6};
     79,54,55,9d,e4,26,cb,23,8c,9a,8a,2e,45,3c,75,8c,ca,d9,17,e1,f4,66,79,2a,51,66,eb,ed,59,32,0f,6f
unsigned char sigd[]={0x93,0x37,0x2D,0xE8,0xDF,0x3C,0x5F,0x35,0xEF,0xB2,0x47,0xE3,0x05,0xFD,0xCA,0xE1,0x8E,0xC3,0x4A,0x65,0x18,0xB3,0xD0,0xB0,0xF2,0xC6,0xA4,0xB8,0xC0,0x46,0xBB,0x57,0x14,0xBD,0xC6,0x4A,0x97,0x3E,0xA2,0x22,0x53,0x1F,0xDB,0xAE,0xF8,0xBB,0x7A,0x55,0xE0,0x5D,0xB6,0x0E,0x47,0xBA,0xD6,0x7B,0xAC,0x9D,0xFD,0x9D,0x38,0x6C,0xAA,0x16};  */
if(strlen(argv[2])>128 || strlen(argv[3])>128 || strlen(argv[4])>128){
	return -1;
}
char char_pubdata[130];
	char char_msg[130];
	char char_sig[130];
	strcpy(char_pubdata,argv[2]);
	strcpy(char_msg,argv[3]);
	strcpy(char_sig,argv[4]);
unsigned char unsigned_pub[strlen(char_pubdata)/2+5]; //= malloc( sizeof(unsigned char)*(strlen(char_pubdata)/2 +1) ); 
unsigned char unsigned_msg[strlen(char_msg)/2+5];
unsigned char unsigned_sig[strlen(char_sig)/2+5];
err_printf("initialize set up\n");
for( i = 0; i < strlen(char_pubdata) ;i+=2) {
	    
    sscanf(char_pubdata+i, "%02hhx", &unsigned_pub[i/2]); // conversion
	err_printf("0x%02X,",unsigned_pub[i/2]);
   //err_printf("%u",tmp_uchr);
    //unsigned_pub[i/2] = tmp_uchr; // save as char

        //assert(1==0);
  }
err_printf("\n");
err_printf("parse1\n");
  for(i=0;i<strlen(char_msg);i+=2){
  	
    sscanf( char_msg+ i, "%02hhx", &unsigned_msg[i/2] ); // conversion
    err_printf("0x%02X,",unsigned_msg[i/2]);
   // unsigned_msg[i/2] = tmp_uchr;
  }
/*for(i=0;i<64;i++){
if(ms[i]!=unsigned_msg[i]){
err_printf("mss %d %u %u",i,ms[i],unsigned_msg[i]);
}
}*/
err_printf("\nparse2\n");
 for(i=0;i<strlen(char_sig);i+=2){
  	
    sscanf( char_sig+ i, "%2x", &unsigned_sig[i/2]); // conversion
    //err_printf("0x%02X,",unsigned_sig[i/2]);
    //unsigned_sig[i/2] =tmp_uchr;
  }
err_printf("\nparse3");

/*for(i=0;i<64;i++){
if(ms[i]!=unsigned_msg[i]){
err_printf("mss %d %u %u",i,ms[i],unsigned_msg[i]);
}
}*/
err_printf("%d\n",signature_verify(unsigned_sig,unsigned_msg,unsigned_pub));
 out_printf("%d\n",signature_verify(unsigned_sig,unsigned_msg,unsigned_pub));
err_printf("%d",sizeof(unsigned_sig));

/*err_printf("%d",signature_verify(sigd,ms,pubd));
err_printf("%d",sizeof(sigd));*/
/*for(i=0;i<64;i++){
if(pubd[i]!=unsigned_pub[i]){
err_printf("pubb");
}
}*/
/*for(i=0;i<64;i++){
if(sigd[i]!=unsigned_sig[i]){
err_printf("sigg %u %u",sigd[i],unsigned_sig[i]);
}
}*/
if(signature_verify(unsigned_sig,unsigned_msg,unsigned_pub)){
	int ret=sign_up(argv[3],argv[2]);
	state_write(&state, sizeof(state));
		state_read(&state, sizeof(state));
	return ret;
}
return -1;
}
return 1;
  /*printf("complete\n");
 for(i=0;i<64;i++){
 	printf("%x",unsigned_pubdata[i]);
 }*/
}

