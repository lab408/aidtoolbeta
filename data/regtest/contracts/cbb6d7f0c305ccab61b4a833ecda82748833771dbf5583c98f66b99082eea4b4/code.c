#include <ourcontract.h>
//sha2 signature uuid
typedef struct {
	char hash[256]; //len
	char address[256]; //len
	
} chainid;

static struct {
	     
	int chainidNums;
	chainid chainids[1000];
} state;

static int find_address(const char *address)
{
	out_printf("0:finding address\n");
	
	int i,j;
	for (i = 0; i < state.chainidNums; i++) {
		err_printf("01:finding address\n");
		
		if (str_cmp(address, state.chainids[i].address, 34) == 0) {
			out_printf("1:address found!!\n");
			return i;
		}
	
	}
	out_printf("1:address finding failed!!\n");
	return -1;
}
static int find_hash(const char *hash)
{
	int i;
	for (i = 0; i < state.chainidNums; i++) {
		
		if (str_cmp(hash, state.chainids[i].hash, 64) == 0) {
			return i;
		
	}
	}

	return -1;
}
 


static int sign_up(const char *hash, const char *address)
{
	int index;
	/* already signed up */
	out_printf("signing up\n");
	out_printf("address: %s\n",address);
	if ((index=find_address(address)) != -1) 
	{
		
		str_printf(state.chainids[index].hash, 256, "%s", hash);
		//str_printf(state.chainids[index].address, 256, "%s",address);
		out_printf("hash: %s\n",state.chainids[index].hash);
		
	}

	
	else
	{
	if (state.chainidNums == 500) return -1;/* number of users reaches upper bound */

	str_printf(state.chainids[state.chainidNums].hash, 256, "%s", hash);
	str_printf(state.chainids[state.chainidNums].address, 256, "%s",address);
	
	//out_printf("%s\n", state.users[state.user_count].name);
	out_printf("hash: %s\n",state.chainids[state.chainidNums].hash);
	state.chainidNums++;
	}
	out_printf("chainidnums: %d\n",state.chainidNums);
	return 0;
}
static int find_address_check_hash(const char *address,const char *hash)
{
	out_printf("finding\n");
	int chainidIndex=find_address(address);
	out_printf("chainid index %d\n",chainidIndex);
	if(chainidIndex==-1){
	return -1;
}
	if(str_cmp(state.chainids[chainidIndex].hash,hash,64)==0)
	{
		out_printf("hash: %s\n",state.chainids[state.chainidNums].hash);
		out_printf("user's hash correct\n");
		return 0;
	}
out_printf("user's hash incorrect\n");
	return -1;
}



static void state_init()
{
	state.chainidNums = 0;
	out_clear();
}

/*
 * argv[0]: contract id
 * argv[1]: subcommand
 * argv[2...]: args
 */
//find_address(const char *address),find_hash(const char *hash),
//sign_up(const char *hash, const char *address),find_address_check_hash(char *address,char *hash)
int contract_main(int argc, char **argv)
{
	if (state_read(&state, sizeof(state)) == -1) {
		/* first time call */
		state_init();
		state_write(&state, sizeof(state));
		state_read(&state, sizeof(state));
	}

	if (argc < 2) {
		err_printf("%s: no subcommand\n", argv[0]);
		return 0;
	}

	/* subcommand "sign_up" */
	if (str_cmp(argv[1], "sign_up", 7) == 0) {
		if (argc != 4) {
			err_printf("%s: usage: sign_up hash address\n", argv[0]);
			return 0;
		}

		int ret = sign_up(argv[2], argv[3]);
		if (ret != 0) {
			err_printf("%s: sign_up failed\n", argv[0]);
			return 0;
		}

		state_write(&state, sizeof(state));
		return 0;
	}

	/* subcommand "find_address" */

	if (str_cmp(argv[1], "find_address_check_hash",23) == 0) {
		if (argc != 4) {
			err_printf("%s: usage: find_address_check_hash address hash\n", argv[0]);
			return 0;
		}
		err_printf("finding\n");
		int ret = find_address_check_hash(argv[2],argv[3]);
		err_printf("%d ret result\n",ret);
		if (ret != 0) {
			err_printf("%s: find_address_check_hash failed\n", argv[0]);
			return 0;
		}

		state_write(&state, sizeof(state));
		return 0;
	}
	

	/* subcommand "freeze" */
	
	return 0;
}

